import { defineConfig } from 'windicss/helpers'

export default defineConfig({
  theme: {
    extend: {
      screens: {
        desktop: '1200px',
      },
    },
  },
})
